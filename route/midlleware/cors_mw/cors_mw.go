package middleware

import (
	"github.com/gin-gonic/gin"
)

func CorsMidlleware() gin.HandlerFunc {
	return func(context *gin.Context) {
		context.Writer.Header().Set("Access-Control-Allow-Origin", "http://macrm.tk")
		context.Writer.Header().Set("Access-Control-Allow-Methods", "OPTIONS,POST")
		context.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		context.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, X-Requested-With")
		context.Next()
	}
}
