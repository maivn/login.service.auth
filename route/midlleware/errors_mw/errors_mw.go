package errors_mw

import (
	"github.com/gin-gonic/gin"
	"login.service.auth/shared/errors"
	"net/http"
)

func ErrorsMidlleware() gin.HandlerFunc {
	return func(context *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				if es, ok := err.(errors.Errors); ok {
					context.AbortWithStatusJSON(http.StatusInternalServerError, es)
				} else if er, ok := err.(error); ok {
					context.AbortWithStatusJSON(http.StatusInternalServerError, er.Error())
				} else {
					context.AbortWithStatusJSON(http.StatusInternalServerError, "Unknown error")
				}
			}
		}()
		context.Next()
	}
}
