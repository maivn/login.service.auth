package route

import (
	"github.com/gin-gonic/gin"
	"login.service.auth/route/midlleware/errors_mw"
	"login.service.auth/controller/register"
	"login.service.auth/route/midlleware/cors_mw"
)

func RegisterRoute(server *gin.Engine)  {
	server.Use(errors_mw.ErrorsMidlleware())
	server.Use(middleware.CorsMidlleware())

	server.POST("/register/contact", register.Contact)
	server.POST("/register/code", register.Code)
	server.POST("/register/password", register.Password)

	server.GET("/register/contact", register.Contact)
	server.GET("/register/code", register.Code)
	server.GET("/register/password", register.Password)

	server.OPTIONS("/register/contact", func(context *gin.Context) {
		context.AbortWithStatus(200)
	})
	server.OPTIONS("/register/code", func(context *gin.Context) {
		context.AbortWithStatus(200)
	})

	server.OPTIONS("/register/password", func(context *gin.Context) {
		context.AbortWithStatus(200)
	})
}