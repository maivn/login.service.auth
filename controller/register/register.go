package register

import (
	"time"
	"github.com/gin-gonic/gin"

	"login.service.auth/model/contact"
	"login.service.auth/shared/binding"
	"login.service.auth/shared/validate"
	"login.service.auth/shared/utils"
	"login.service.auth/shared/redis"
	"login.service.auth/shared/json"
	"login.service.auth/shared/cookie"
	"login.service.auth/model/user"
	"login.service.auth/shared/response"
	"fmt"
)

func Contact(context *gin.Context) {
	var Contact contact.Contact
	binding.Fill(context, &Contact)
	validate.ValidateValueWithType(&Contact)
	Contact.NotExist()
	uuid := utils.GenerateUuid()
	code := utils.GenerateDigitalCode(6)
	Contact.Code = code
	redis.Set(uuid, json.Serialize(&Contact), 10*60*time.Second)
	cookie.Set(context, "cid", uuid, 10*60)
	context.JSON(200, response.Response{
		uuid,
	})
}

func Code(context *gin.Context) {
	var redisContact contact.Contact
	uuid := cookie.Get(context, "cid")
	json.Deserialize(redis.Get(uuid), &redisContact)
	var Contact contact.Contact
	binding.Fill(context, &Contact)
	validate.ValidateStructPartial(&Contact, "Code")
	redisContact.Confirm(Contact.Code)
	redis.Set(uuid, json.Serialize(&redisContact), 10*60*time.Second)
	cookie.Set(context, "cid", uuid, 10*60)
	context.JSON(200, response.Response{
		uuid,
	})
}

func Password(context *gin.Context) {
	var redisContact contact.Contact
	uuid := cookie.Get(context, "cid")
	json.Deserialize(redis.Get(uuid), &redisContact)
	redisContact.IsConfirmed()
	redisContact.NotExist()
	var User user.User
	binding.Fill(context, &User)
	fmt.Println(User)
	validate.ValidateStruct(&User)
	User.Create()
	redisContact.UserId = User.ID
	redisContact.Create()
	context.JSON(200, response.Response{User.ID})
}

func Check(context *gin.Context) {
	uuid := cookie.Get(context, "cid")
	value := redis.Get(uuid)
	var Contact contact.Contact
	json.Deserialize(value, &Contact)
	Contact.IsConfirmed()
	context.JSON(200, response.Response{
		uuid,
	})
}
