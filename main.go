package main

import (
	"github.com/gin-gonic/gin"
	"login.service.auth/shared/config"
	"login.service.auth/shared/redis"
	"os"
	"login.service.auth/route"
	"login.service.auth/shared/database"
	"login.service.auth/model/user"
	"login.service.auth/model/contact"
)

func init() {
	//config.Load("./dev.env")
	config.Load()
	redis.Init()
	database.Init()
	database.Sql.AutoMigrate(user.User{}, contact.Contact{})
}

func main() {
	server := gin.Default()
	route.RegisterRoute(server)
	server.Run(":" + os.Getenv("APP_PORT"))
}
