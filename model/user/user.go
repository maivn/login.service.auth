package user

import (
	"github.com/jinzhu/gorm"
	"login.service.auth/shared/database"
	e "login.service.auth/shared/errors"
)

type User struct {
	gorm.Model
	Name     string `form:"name"`
	Password string `form:"password" validate:"required,len=6"`
	Confirm  string `form:"confirm" validate:"eqfield=Password"`
}

func (user *User) Create() {
	if err := database.Sql.Save(user).Error; err != nil {
		e.Errs.Add(&e.Error{Code: e.ServerErrorDatabaseCreate, SubCode: "user"}).Exist()
	}
}
