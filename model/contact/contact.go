package contact

import (
	"github.com/jinzhu/gorm"

	e "login.service.auth/shared/errors"
	"login.service.auth/shared/database"
)

type Contact struct {
	gorm.Model
	UserId    uint
	Type      string `form:"type" validate:"required,eq=phone|eq=email"`
	Value     string `form:"value" validate:"required"`
	Code      string `form:"code" validate:"required,len=6,numeric"`
	Confirmed bool
}

func (contact *Contact) Confirm(code string) {
	if contact.Code != code {
		e.Errs.Add(&e.Error{Code: e.ClientErrorConfirmCode}).Exist()
	}
	contact.Confirmed = true
}

func (contact *Contact) IsConfirmed() {
	if !contact.Confirmed {
		e.Errs.Add(&e.Error{Code: e.ClientErrorContactNotConfirmed}).Exist()
	}
}

func (contact *Contact) Create() {
	if err := database.Sql.Save(contact).Error; err != nil {
		e.Errs.Add(&e.Error{Code: e.ServerErrorDatabaseCreate, SubCode: "contact"}).Exist()
	}
}

func (contact *Contact) Exist() {
	if database.Sql.
		Where("type=?", contact.Type).
		Where("value=?", contact.Value).First(&Contact{}).RecordNotFound() {
		e.Errs.Add(&e.Error{Code: e.ClientErrorUserAndContactNotExist}).Exist()
	}
}
func (contact *Contact) NotExist() {
	if !database.Sql.
		Where("type=?", contact.Type).
		Where("value=?", contact.Value).First(&Contact{}).RecordNotFound() {
		e.Errs.Add(&e.Error{Code: e.ClientErrorUserAndContactExist}).Exist()
	}
}
