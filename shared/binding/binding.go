package binding

import (
	"github.com/gin-gonic/gin"
	e "login.service.auth/shared/errors"
)

func Fill(context *gin.Context, entity interface{}) {
	if err := context.Bind(entity); err != nil {
		e.Errs.Add(&e.Error{Code: e.ServerErrorInternal}).Exist()
	}
}
