package validate

import (
	"login.service.auth/model/contact"
	"strings"
)

func ValidateValueWithType(contact *contact.Contact) {
	ValidateStructPartial(contact, "Type", "Value")
	ValidateField(contact.Value, strings.ToLower(contact.Type))
}
