package validate

import (
	"gopkg.in/go-playground/validator.v9"
	e "login.service.auth/shared/errors"
)

var Validate *validator.Validate

func init() {
	Validate = validator.New()
	Validate.RegisterValidation("phone", phoneValidationFunc)
}

func phoneValidationFunc(fl validator.FieldLevel) bool {
	return false
}

func ValidateStruct(entity interface{}) {
	err := Validate.Struct(entity)
	if err != nil {
		e.Errs.Add(&e.Error{Code: e.ClientErrorValidation, Message:err.Error()}).Exist()
	}
}

func ValidateStructPartial(entity interface{}, fields ...string) {
	for _, field := range fields {
		err := Validate.StructPartial(entity, field)
		if err != nil {
			e.Errs.Add(&e.Error{Code: e.ClientErrorValidation, SubCode: field, Message: err.Error()})
		}
	}
	e.Errs.Exist()
}

func ValidateField(field interface{}, tag string)  {
	err := Validate.Var(field, tag)
	if err != nil {
		e.Errs.Add(&e.Error{Code: e.ClientErrorValidation, SubCode: tag}).Exist()
	}
}