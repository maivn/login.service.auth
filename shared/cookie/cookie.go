package cookie

import (
	"github.com/gin-gonic/gin"
	e "login.service.auth/shared/errors"
	"fmt"
)

func Set(context *gin.Context, name string, value string, maxAge int) {
	context.SetCookie(name, value, maxAge, "/", ".macrm.tk", false, false)
}

func Get(context *gin.Context, name string) string {
	fmt.Println(name)
	fmt.Println(context.Cookie("cid"))
	value, err := context.Cookie(name)
	if err != nil {
		e.Errs.Add(&e.Error{Code: e.ClientErrorCookieGet}).Exist()
	}
	return value
}

func GetFunc(context *gin.Context, name string, f func(errs ...error)) string {
	value, err := context.Cookie(name)
	if err != nil {
		e.Errs.Add(&e.Error{Code: e.ClientErrorCookieGet}).Exist()
	}
	return value
}
