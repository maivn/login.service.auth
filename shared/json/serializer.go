package json

import (
	"encoding/json"
	e "login.service.auth/shared/errors"
)

func Serialize(i interface{}) string {
	bytes, err := json.Marshal(i)
	if err != nil {
		e.Errs.Add(&e.Error{Code: e.ServerErrorJsonSerialize}).Exist()
	}
	return string(bytes)
}

func Deserialize(str string, i interface{}) {
	err := json.Unmarshal([]byte(str), i)
	if err != nil {
		e.Errs.Add(&e.Error{Code: e.ServerErrorJsonDeserialize}).Exist()
	}
}
