package redis

import (
	"os"
	"github.com/go-redis/redis"
	"time"
	e "login.service.auth/shared/errors"
	"fmt"
)

var Client *redis.Client

func Init() {
	Client = redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_HOST") + ":" + os.Getenv("REDIS_PORT"),
		Password: os.Getenv("REDIS_PASSWORD"), // no password set
		DB:       0,  // use default DB
	})
	_, err := Client.Ping().Result()
	if err != nil {
		panic(err.Error())
	}
}

func Set(key string, value string, duration time.Duration)  {
	fmt.Println(key, value, duration)
	err := Client.Set(key, value, duration).Err()
	if err != nil {
		e.Errs.Add(&e.Error{Code: e.ServerErrorRedisSet}).Exist()
	}
}

func Get(key string) string {
	value, err := Client.Get(key).Result()
	if err != nil {
		e.Errs.Add(&e.Error{Code: e.ServerErrorRedisGet}).Exist()
	}
	return value
}