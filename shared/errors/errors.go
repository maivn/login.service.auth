package errors

type Error struct {
	Code    int
	SubCode string
	Message string
}

type Errors []*Error

var Errs Errors

func (es *Errors) Add(e *Error) *Errors {
	e.fillMessageByCode()
	*es = append(*es, e)
	return es
}

func (es *Errors) Exist() {
	if len((*es)) != 0 {
		panic(*es.remove())
	}
}

func (es *Errors) remove() *Errors {
	var errors = *es
	*es = (*es)[:0]
	return &errors
}

func (e *Error) fillMessageByCode() Error {
	if msg, ok := messagesErrors[e.Code]; ok && e.Message == "" {
		e.Message = msg
	}
	return *e
}

const (
	ClientErrorBadRequest = 4000
	ClientErrorValidation = 4001
	ClientErrorCookieGet = 4002
	ClientErrorConfirmCode = 4003
	ClientErrorContactNotConfirmed = 4004
	ClientErrorUserAndContactExist = 4005
	ClientErrorUserAndContactNotExist = 4006

	ServerErrorInternal = 5000
	ServerErrorBinding  = 5001
	ServerErrorRedisSet  = 5002
	ServerErrorRedisGet  = 5003
	ServerErrorJsonSerialize = 5004
	ServerErrorJsonDeserialize = 5005
	ServerErrorDatabaseCreate = 5006
)

var messagesErrors = map[int]string{
	ClientErrorBadRequest: "Client error bad request",
	ClientErrorValidation: "Client error validation",
	ClientErrorCookieGet: "Client error cookie get",
	ClientErrorConfirmCode: "Client error confirm code",
	ClientErrorContactNotConfirmed: "Client error contact not confirmed",
	ClientErrorUserAndContactExist:"Client error user and contact exist",
	ClientErrorUserAndContactNotExist:"Client error user and contact not exist",

	ServerErrorInternal: "Server error internal",
	ServerErrorBinding:  "Server error binding",
	ServerErrorRedisSet: "Server error redis set",
	ServerErrorRedisGet: "Server error redis get",
	ServerErrorJsonSerialize: "Server error json serialize",
	ServerErrorJsonDeserialize: "Server error json deserialize",
	ServerErrorDatabaseCreate: "Server error database create",
}
